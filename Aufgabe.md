# Aufgabe

Hier ist ein exemplarisches Playbook und ein Inventory, das Variablen verwendet, aber nicht konsequent optimiert ist, was zu Redundanzen führt:

### Inventory

```ini
[web]
web1 ansible_host=192.168.1.10
web2 ansible_host=192.168.1.11

[db]
db1 ansible_host=192.168.1.20
db2 ansible_host=192.168.1.21

[all:vars]
ansible_user=admin
ansible_ssh_private_key_file=/path/to/private/key
http_port=80
```

### Playbook

```yaml
---
- hosts: web
  tasks:
    - name: Install Apache
      apt:
        name: apache2
        state: present
      become: yes

    - name: Copy index.html to web servers
      copy:
        src: /home/admin/index.html
        dest: /var/www/html/index.html
      become: yes

    - name: Ensure Apache is running
      service:
        name: apache2
        state: started
      become: yes

- hosts: db
  tasks:
    - name: Install MySQL
      apt:
        name: mysql-server
        state: present
      become: yes

    - name: Ensure MySQL is running
      service:
        name: mysql
        state: started
      become: yes
```

### Problemstellen:
1. **Redundante Variablen**: Im `copy`-Task wird der Pfad hartkodiert, obwohl man dafür eine Variable definieren könnte.
2. **Redundante Tasks**: Mehrere Tasks haben die gleiche `become: yes`-Anweisung, die stattdessen global konfiguriert werden könnte.
3. **Fehlende Nutzung von Variablen**: Zum Beispiel könnte der HTTP-Port variabilisiert werden.

Diese Struktur enthält genug Redundanzen, um sie später zu optimieren, z. B. durch Variablen-Nutzung und effizientere Gruppierung der Konfigurationen.