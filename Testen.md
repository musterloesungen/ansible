## Test

Für Ansible gibt es ein standardisiertes Testverfahren, das vergleichbar mit Unit Tests in Java ist. Das gängigste Tool dafür ist **Molecule**, das speziell entwickelt wurde, um Ansible-Rollen und Playbooks zu testen. Molecule ermöglicht es dir, verschiedene Szenarien zu definieren, um sicherzustellen, dass dein Playbook oder deine Rolle korrekt ausgeführt wird. Es bietet dir auch die Möglichkeit, Tests in verschiedenen Umgebungen durchzuführen (z. B. Docker, Vagrant, oder Cloud-Instanzen) und definiert damit eine klare Testpipeline ähnlich wie Unit Tests.

### Wie funktioniert Molecule?

1. **Initialisierung**: Molecule wird in dein Projekt integriert und generiert eine Verzeichnisstruktur, die Konfigurationsdateien und Test-Szenarien enthält.
2. **Tests schreiben**: Du kannst Testfälle erstellen, um sicherzustellen, dass bestimmte Tasks erfolgreich ausgeführt werden, Services laufen oder Dateien vorhanden sind.
3. **Ausführen der Tests**: Molecule führt das Playbook in einer isolierten Umgebung (z. B. in einem Docker-Container) aus und verifiziert, ob die erwarteten Ergebnisse erreicht werden.
4. **Löschen der Umgebung**: Nach dem Test wird die Umgebung bereinigt, sodass du in einer sauberen Umgebung testen kannst.

### Beispiel einer Test-Pipeline mit Molecule

1. **Molecule installieren**
   
   Installiere Molecule und Docker (falls noch nicht vorhanden):

   ```bash
   pip install molecule docker
   ```

2. **Rolle mit Molecule initialisieren**

   Angenommen, du hast eine Rolle namens `apache`. Initialisiere Molecule in dieser Rolle:

   ```bash
   cd roles/apache
   molecule init scenario --driver-name docker
   ```

   Dies erstellt die Verzeichnisstruktur für deine Tests.

3. **Verzeichnisstruktur von Molecule**

   Molecule erstellt ein `molecule/` Verzeichnis in deiner Rolle mit einer Struktur wie:

   ```
   molecule/
   └── default/
       ├── converge.yml     # Playbook, das getestet wird
       ├── molecule.yml     # Testkonfiguration (z. B. Docker als Umgebung)
       └── verify.yml       # Testfälle (Verifikation)
   ```

4. **Testfälle schreiben**

   In der `verify.yml` kannst du Verifikationen schreiben, um sicherzustellen, dass Tasks korrekt ausgeführt wurden. Zum Beispiel:

   ```yaml
   ---
   - name: Verify Apache is installed and running
     hosts: all
     tasks:
       - name: Check if Apache is installed
         command: apache2 -v
         register: apache_version
         changed_when: false
         failed_when: apache_version.rc != 0

       - name: Verify Apache service is running
         service_facts:
       - name: Is Apache running?
         assert:
           that: "'apache2' in ansible_facts.services"
   ```

5. **Tests ausführen**

   Führe Molecule aus, um die Tests in einer Docker-Umgebung durchzuführen:

   ```bash
   molecule test
   ```

   Molecule startet eine Docker-Instanz, wendet das Playbook an, führt die Tests durch und bereinigt die Umgebung danach.

### Vorteile von Molecule-Tests:
- **Isolierte Umgebung**: Du testest dein Playbook oder deine Rolle in einer frischen Umgebung, was eine hohe Zuverlässigkeit der Tests sicherstellt.
- **Automatisierung**: Molecule kann in CI/CD-Pipelines integriert werden, um sicherzustellen, dass Änderungen an deinen Ansible-Playbooks automatisch getestet werden, bevor sie in Produktion gehen.
- **Skalierbarkeit**: Du kannst verschiedene Test-Szenarien erstellen, z. B. verschiedene Betriebssysteme oder Konfigurationen simulieren.

### Vergleich mit Unit Tests:
Ähnlich wie bei Unit Tests in Java, wo du den Zustand einer Methode oder Klasse überprüfst, testest du bei Ansible mithilfe von Molecule, ob eine Rolle oder ein Playbook bestimmte Aufgaben korrekt ausführt. Während Unit Tests spezifische Methoden verifizieren, prüfen Molecule-Tests die Zustände und Ergebnisse von Konfigurationen in einer Umgebung.

### tl;dr
Wenn du Ansible-Projekte professionell einsetzt und sicherstellen möchtest, dass Änderungen keine unerwarteten Probleme verursachen, lohnt sich der Einsatz von Molecule für automatisierte Tests. Es bietet eine standardisierte Testpipeline, um die Qualität und Konsistenz deiner Playbooks und Rollen zu garantieren.