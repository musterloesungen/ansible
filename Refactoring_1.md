## Refactoring 1

Es ist ein sinnvolles Vorgehen, erst kleinteilig die Redundanzen zu entfernen und Variablen einzuführen. 

Das erste Refactoring-Level räumt die häufigsten Redundanzen auf, macht das Playbook flexibler und leichter wartbar.

### Refakturiertes Inventory

```ini
[web]
web1 ansible_host=192.168.1.10
web2 ansible_host=192.168.1.11

[db]
db1 ansible_host=192.168.1.20
db2 ansible_host=192.168.1.21

[all:vars]
ansible_user=admin
ansible_ssh_private_key_file=/path/to/private/key
http_port=80
document_root=/var/www/html
```

### Refakturiertes Playbook

```yaml
---
- hosts: all
  become: yes
  tasks:
    - name: Set common permissions for users
      file:
        path: /home/{{ ansible_user }}
        owner: "{{ ansible_user }}"
        group: "{{ ansible_user }}"
        mode: '0755'

- hosts: web
  tasks:
    - name: Install Apache
      apt:
        name: apache2
        state: present

    - name: Copy index.html to web servers
      copy:
        src: /home/{{ ansible_user }}/index.html
        dest: "{{ document_root }}/index.html"

    - name: Ensure Apache is running
      service:
        name: apache2
        state: started

- hosts: db
  tasks:
    - name: Install MySQL
      apt:
        name: mysql-server
        state: present

    - name: Ensure MySQL is running
      service:
        name: mysql
        state: started
```

### Verbesserungen:
1. **`become: yes` global definiert**: Dies spart Wiederholungen in jedem Task.
2. **Variablen eingeführt**: Zum Beispiel für den `document_root`-Pfad und den Benutzer-Ordner.
3. **Redundanzen entfernt**: Hartkodierte Pfade wurden durch Variablen ersetzt.

