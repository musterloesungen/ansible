# Copy Module

Hier ist der Text in einer neutralen Form umformuliert:

---

Das Ansible `copy`-Modul zeigt bei der Übertragung von kompletten Verzeichnissen mit großen Inhalten oder tiefen Strukturen Schwächen. Ein effizienter Ersatz ist das `synchronize`-Modul, ein Wrapper für `rsync`, das speziell für solche Anwendungsfälle geeignet ist.

### Schritte zur Migration vom `copy`- zum `synchronize`-Modul:

1. **Unterschiede zwischen den Modulen:**
   - **`copy`-Modul:** Gut für das Kopieren einzelner Dateien oder kleiner Verzeichnisse. Bei größeren Datenmengen ineffizient und bei tiefen Verzeichnisstrukturen problematisch.
   - **`synchronize`-Modul:** Nutzt `rsync` und ist besonders für große und komplexe Verzeichnisse geeignet, da es effizienter und zuverlässiger arbeitet.

2. **Anpassung des Playbooks:**
   - **Vorher mit dem `copy`-Modul:**
     ```yaml
     - name: Kopiere das Verzeichnis auf den Zielserver
       copy:
         src: /local/path/to/directory/
         dest: /remote/path/to/directory/
         owner: someuser
         group: somegroup
         mode: '0755'
         recursive: yes
     ```
   - **Nachher mit dem `synchronize`-Modul:**
     ```yaml
     - name: Synchronisiere das Verzeichnis auf den Zielserver
       synchronize:
         src: /local/path/to/directory/
         dest: /remote/path/to/directory/
         archive: yes
         delete: yes
     ```

3. **Wichtige Parameter des `synchronize`-Moduls:**
   - **`src`**: Quellpfad auf dem Controller (lokal) oder Remote-Host, je nach Modus.
   - **`dest`**: Zielpfad auf dem Remote-Host oder Controller.
   - **`mode`**: Bestimmt die Richtung (`push` oder `pull`), Standard ist `push`.
   - **`archive`**: Bewahrt Berechtigungen, Zeitstempel und symbolische Links.
   - **`delete`**: Löscht Dateien auf dem Ziel, die nicht mehr im Quellverzeichnis vorhanden sind.

4. **Sicherheitsaspekte:**
   - **SSH-Schlüssel:** Es wird sichergestellt, dass passwortlose SSH-Schlüssel zwischen Controller und Zielhosts korrekt eingerichtet sind.
   - **Benutzerberechtigungen:** Der Benutzer sollte die notwendigen Rechte auf beiden Seiten haben.
   - **Firewall-Einstellungen:** Da `rsync` über SSH läuft, werden die entsprechenden Ports offengehalten.

5. **Tests durchführen:**
   - Mit `--dry-run` lassen sich Trockenläufe ausführen, um zu sehen, welche Änderungen vorgenommen würden.
   - Durch den Einsatz von `check_mode` in Ansible können Playbooks getestet werden, ohne tatsächliche Änderungen vorzunehmen.

6. **Rollback-Strategie:**
   - Es ist ratsam, Backups wichtiger Daten zu erstellen, bevor Änderungen vorgenommen werden.
   - Änderungen werden dokumentiert, um bei Bedarf schnell zurückkehren zu können.

7. **Weitere Optimierungen:**
   - **Benutzerdefinierte `rsync`-Optionen**: Mit `rsync_opts` können weitere Optionen übergeben werden.
     ```yaml
     - name: Synchronisiere mit benutzerdefinierten rsync-Optionen
       synchronize:
         src: /local/path/
         dest: /remote/path/
         rsync_opts:
           - "--chmod=D0755,F0644"
           - "--exclude='.git/'"
     ```
   - **Geschwindigkeitsbegrenzung:** Mit `bwlimit` lässt sich die Bandbreitennutzung steuern.

8. **Anpassung der Variablen und Rollen:**
   - Variablen, die Pfade oder Berechtigungen betreffen, werden aktualisiert.
   - Rollen werden angepasst, um das `synchronize`-Modul zu nutzen.

**Beispiel eines aktualisierten Tasks mit dem `synchronize`-Modul:**

```yaml
- name: Synchronisiere das Verzeichnis auf den Remote-Host
  synchronize:
    src: /local/path/to/directory/
    dest: /remote/path/to/directory/
    archive: yes
    delete: yes
    rsync_opts:
      - "--compress"
      - "--omit-dir-times"
  become: yes
  become_user: someuser
```

**Wichtige Hinweise:**

- **Direkter Austausch des Moduls:** Es wird nicht einfach nur das Modul ausgetauscht, sondern auch die spezifischen Parameter angepasst, um das gewünschte Verhalten sicherzustellen.
- **Optionen überprüfen:** Einige Optionen des `copy`-Moduls haben keine direkte Entsprechung im `synchronize`-Modul.
- **Pfadangaben:** Auf die korrekte Verwendung von abschließenden Schrägstrichen (`/`) bei Pfadangaben wird geachtet, da dies das Verhalten von `rsync` beeinflusst.
- **Logs und Feedback:** Das `synchronize`-Modul liefert detailliertere Feedback-Meldungen, was bei der Fehlerbehandlung hilfreich sein kann.

**Sicherheitsüberlegungen:**

- **Vertrauenswürdige Hosts:** Es wird darauf geachtet, dass nur zu vertrauenswürdigen Hosts synchronisiert wird, da `rsync` bei falscher Anwendung zu Datenverlust führen kann.
- **Anmeldung und Authentifizierung:** Sichere Methoden für die Authentifizierung, idealerweise SSH-Schlüssel mit Passphrasen oder andere sichere Verfahren, werden verwendet.
