## Refactoring 2

Der nächste Level des Refactorings könnte sich auf eine tiefere Modularisierung und Wiederverwendbarkeit konzentrieren. Folgende Punkte könnten umgesetzt werden:

1. **Modularisierung durch Rollen**: Playbooks sollten in Rollen aufgeteilt werden, um bestimmte Aufgaben wie das Installieren von Apache oder MySQL in separaten, wiederverwendbaren Rollen zu organisieren.
2. **Variablen in separate Dateien auslagern**: Variablen für Web- und Datenbankserver könnten in separate Dateien ausgelagert werden, um eine klarere Struktur zu erhalten.
3. **Bessere Verwendung von Handlern**: Anstelle von Tasks wie "Ensure Apache is running" in jedem Playbook, könnten Handler verwendet werden, um diese Aktionen nur dann auszuführen, wenn sie nötig sind (z. B. wenn die Apache-Installation verändert wurde).
4. **Gruppenvariablen für spezifische Servergruppen**: Gruppen wie `web` und `db` könnten spezifische Variablen haben, die ihre jeweilige Konfiguration betreffen.

Dieses Level ermöglicht eine noch effizientere und skalierbare Nutzung von Ansible, da es die Wartbarkeit des Playbooks erheblich verbessert.

### Modularisierung durch Rollen

#### Verzeichnisstruktur

```
.
├── inventories
│   └── production
│       └── hosts.ini
├── playbooks
│   └── site.yml
└── roles
    ├── apache
    │   ├── tasks
    │   │   └── main.yml
    │   ├── handlers
    │   │   └── main.yml
    │   └── vars
    │       └── main.yml
    └── mysql
        ├── tasks
        │   └── main.yml
        ├── handlers
        │   └── main.yml
        └── vars
            └── main.yml
```

### Beispiel Playbook `site.yml`

```yaml
---
- hosts: web
  roles:
    - apache

- hosts: db
  roles:
    - mysql
```

### Beispiel Rolle `apache` (tasks/main.yml)

```yaml
---
- name: Install Apache
  apt:
    name: apache2
    state: present

- name: Copy index.html to web servers
  copy:
    src: /home/{{ ansible_user }}/index.html
    dest: "{{ document_root }}/index.html"

- name: Ensure Apache is running
  service:
    name: apache2
    state: started
```

### Handler in `apache` (handlers/main.yml)

```yaml
---
# Keine Handler erforderlich
```

### Variablen in `apache` (vars/main.yml)

```yaml
---
document_root: /var/www/html
```

### Rolle `mysql` (tasks/main.yml)

```yaml
---
- name: Install MySQL
  apt:
    name: mysql-server
    state: present

- name: Ensure MySQL is running
  service:
    name: mysql
    state: started
  notify: Restart MySQL
```

### Handler in `mysql` (handlers/main.yml)

```yaml
---
- name: Restart MySQL
  service:
    name: mysql
    state: restarted
```

### Variablen in `mysql` (vars/main.yml)

```yaml
---
mysql_root_password: secret
```

### Vorteile dieses Levels:

1. **Wiederverwendbare Rollen**: Jede Komponente (Apache, MySQL) ist jetzt in einer eigenen Rolle organisiert, was die Wiederverwendbarkeit für andere Projekte oder Servergruppen erhöht.
2. **Variablen-Dateien pro Rolle**: Variablen sind jetzt klar in die jeweiligen Rollen aufgeteilt, was die Übersichtlichkeit verbessert.
3. **Handlers nutzen**: Handler stellen sicher, dass Services nur dann neu gestartet werden, wenn tatsächlich Änderungen vorgenommen wurden (z. B. bei einer neuen Konfiguration).