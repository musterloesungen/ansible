## Refactoring 3

Ein weiteres sinnvolles Level wäre das Hinzufügen von Fehlerbehandlung und Robustheit.

Dieses Level bringt zusätzliche Zuverlässigkeit und sorgt dafür, dass dein Playbook auch in problematischen Situationen stabil bleibt und flexibel auf Fehler reagiert.

Hier sind einige Ansätze, die das Playbook auf ein fortgeschritteneres Niveau bringen könnten:

1. **Fehlerbehandlung und Bedingte Ausführung**: Die Verwendung von `failed_when`, `ignore_errors` oder bedingten Anweisungen mit `when` kann sicherstellen, dass das Playbook auch in Situationen funktioniert, in denen bestimmte Aufgaben fehlschlagen könnten, ohne den gesamten Ablauf zu unterbrechen.

2. **Verwendung von `block` für Aufgabenblöcke**: Dies erlaubt die Gruppierung von Tasks, sodass, wenn ein Task fehlschlägt, bestimmte Aktionen wie ein Rollback durchgeführt werden können.

3. **Erzwingen bestimmter Voraussetzungen**: Die Nutzung von `pre_tasks`, um sicherzustellen, dass bestimmte Abhängigkeiten (z. B. eine Internetverbindung oder der Zugang zu einem Repository) vor dem Ausführen des Haupt-Playbooks vorhanden sind.

4. **Rückgabewerte und Register verwenden**: Mit `register` können Rückgabewerte von Tasks gespeichert werden, um sie in späteren Tasks zu verwenden. Dies kann nützlich sein, um auf der Grundlage von Zuständen zu agieren, wie z. B. ob ein Service gestartet wurde oder ob eine Datei existiert.

Hier ein Beispiel für eine erweiterte Fehlerbehandlung und Bedingungskontrolle:

### Beispiel `site.yml`

```yaml
---
- hosts: web
  become: yes
  pre_tasks:
    - name: Check for internet connection
      command: ping -c 1 8.8.8.8
      register: internet_check
      failed_when: "'0% packet loss' not in internet_check.stdout"
      ignore_errors: yes

    - name: Fail if no internet connection is found
      fail:
        msg: "No internet connection found. Aborting!"
      when: internet_check.failed

  roles:
    - apache

- hosts: db
  become: yes
  roles:
    - mysql
```

### Erweiterte Fehlerbehandlung im Task

```yaml
---
- name: Install Apache
  apt:
    name: apache2
    state: present
  register: apache_install
  failed_when: apache_install.rc != 0

- name: Rollback Apache if installation fails
  apt:
    name: apache2
    state: absent
  when: apache_install.failed

- name: Copy index.html to web servers
  copy:
    src: /home/{{ ansible_user }}/index.html
    dest: "{{ document_root }}/index.html"
  ignore_errors: yes  # Fehler wird ignoriert, aber protokolliert

- name: Ensure Apache is running
  service:
    name: apache2
    state: started
  notify: Restart Apache
  when: not apache_install.failed
```

### Verwendung von `block` für Rollback

```yaml
---
- block:
    - name: Install MySQL
      apt:
        name: mysql-server
        state: present

    - name: Ensure MySQL is running
      service:
        name: mysql
        state: started

  rescue:
    - name: Rollback MySQL installation
      apt:
        name: mysql-server
        state: absent

  always:
    - name: Notify admin of MySQL status
      debug:
        msg: "MySQL was either successfully installed or rolled back."
```

### Vorteile dieses Levels:

1. **Fehlerrobustheit**: Durch die Verwendung von `failed_when` und `ignore_errors` kann das Playbook besser mit Fehlern umgehen, ohne den gesamten Prozess zu unterbrechen.

2. **Rollback-Mechanismen**: Mit der Verwendung von `block`-Strukturen können bei Fehlern Rollbacks oder Korrekturmaßnahmen durchgeführt werden, um den Zustand des Systems stabil zu halten.

3. **Vorbedingungen prüfen**: Mit `pre_tasks` kann sichergestellt werden, dass das Playbook nur ausgeführt wird, wenn alle nötigen Voraussetzungen erfüllt sind.